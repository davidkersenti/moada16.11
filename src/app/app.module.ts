import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InvoiceComponent } from './invoice/invoice.component';
import{AngularFireModule} from 'angularfire2';
import { InvoicesService } from './invoices/invoices.service';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UserFormComponent } from './user-form/user-form.component';


export const firebaseConfig = {
    apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}


const appRoutes: Routes = [

  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent },
  { path: 'users', component: UsersComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];
 
@NgModule({
  declarations: [
    AppComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    PageNotFoundComponent,
    InvoiceComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    UserFormComponent,
    
    
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
    
  ],
  providers: [UsersService,InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }